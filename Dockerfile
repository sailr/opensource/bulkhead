FROM golang:1.12.5 as builder

WORKDIR /workspace
COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download

COPY bulkhead.go bulkhead.go
COPY pkg/ pkg/

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -a -o bulkhead bulkhead.go

FROM alpine
WORKDIR /
RUN apk add --update bash
COPY --from=builder /workspace/bulkhead .

EXPOSE 3030

CMD ["./bulkhead"]
