# Bulkhead

An API Gateway with Exponential Backoff for Distributed Architectures

## Prelude

An Exponential Backoff is an algorithm that uses feedback to decrease the rate of some process [wiki](https://en.wikipedia.org/wiki/Exponential_backoff).

In more practical terms, an exponential backoff is a [ticker](https://golang.org/pkg/time/#Ticker) that uses feedback from the system to increase its timeout.

Within distributed systems, this is a useful paradigm because it can be enforced under load, when one or two services might be failing because of the increased load.

An exponential backoff eases the rate of request on the struggling service allowing operations teams to increase the horizontal or vertical scaling of that service.

We'll use a slightly modified version of the exponential-backoff algorithm and implement it as a proxy service.

## Implementation




## Epilogue

A theme when building micro services or distributed systems is to have a few services that bear the brunt of the work, and other systems that operator just outside these systems doing operational work to help these main services perform their job. Often, under load, these system queue or slow. This is a micro service anti-patter. A code smell to hint a redesign is required.

Is it ironic that you're only allowed to design once; arguably, when you have the least amount of information to operate.
