# Micro Service Anti-Patterns

Micro services without design thinking is a horse without reins. You can still ride but you're going to be playing fast and loose trying to hold on.

## Distributed Systems are All the Hype

It's true. Distributed systems are the coolest of cool.


## Designing Distributed Systems

Designing distributed system is about cutting away 
