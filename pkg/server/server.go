package server

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

// Start starts the API server
func Start() {
	r := mux.NewRouter()

	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, World")
	})

	log.Println("Listening on 3030")
	http.ListenAndServe(":3030", r)
}
