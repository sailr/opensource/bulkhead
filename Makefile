build:
	go build -o bin

docker-build:
	docker build -t registry.gitlab.com/sailr/opensource/bulkhead .

docker-run:
	docker run -p 3030:3030 -it registry.gitlab.com/sailr/opensource/bulkhead bash

docker-push:
	docker push registry.gitlab.com/sailr/opensource/bulkhead
